@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card rounded-0 rounded-top">
                    <div class="card-header d-flex justify-content-between align-items-center">
                        <div class="fw-bold fs-5">
                            {{ __('Bots') }}
                        </div>
                        <a href="{{ route('bots.create') }}" class="btn btn-success"><i class="fa fa-plus"></i> Add Bot</a>
                    </div>

                    <div class="card-body p-0">
                        @if (session('status'))
                            <div class="alert alert-success rounded-0 m-0" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif

                        <table class="table table-striped table-hover m-0">
                            <thead class="table-dark">
                                <tr>
                                    <th class="text-center" style="width: 5rem;">No.</th>
                                    <th>UID</th>
                                    <th>Bot ID</th>
                                    <th>Bot Name</th>
                                    <th>Bot Username</th>
                                    <th>Subscriber(s)</th>
                                    <th class="text-center" style="width: 15rem;">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @php
                                    $i = 1;
                                @endphp
                                @forelse ($records as $record)
                                    <tr>
                                        <td class="text-center">{{ $i }}</td>
                                        <td>{{ $record->uid }}</td>
                                        <td>{{ $record->bot_id }}</td>
                                        <td>{{ $record->bot_name }}</td>
                                        <td>{{ $record->bot_username }}</td>
                                        <td><a href="{{ route('bots.subscribers', $record->id) }}">{{ $record->users->count()  }}</a></td>
                                        <td class="text-center">
                                            <a href="{{ route('bots.show', $record->id) }}" class="btn btn-secondary btn-sm"><i class="fa fa-eye"></i></a>
                                            <a href="{{ route('bots.edit', $record->id) }}" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>
                                            <a
                                                href="{{ route('bots.destroy', $record->id) }}"
                                                class="btn btn-danger btn-sm"
                                                onclick="event.preventDefault();document.getElementById('delete-{{$record->id}}').submit();"
                                            >
                                                <i class="fa fa-trash"></i>
                                            </a>
                                            <form
                                                id="delete-{{$record->id}}"
                                                action="{{ route('bots.destroy', $record->id) }}"
                                                method="post"
                                                class="d-none"
                                            >
                                                @csrf
                                                @method('DELETE')
                                            </form>
                                        </td>
                                    </tr>
                                    @php
                                        $i++;
                                    @endphp
                                @empty
                                    <tr>
                                        <td colspan="99" class="text-center text-muted fst-italic">No Data.</td>
                                    </tr>
                                @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
