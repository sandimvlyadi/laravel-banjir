@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card rounded-0 rounded-top">
                    <div class="card-header d-flex justify-content-between align-items-center">
                        <div class="fw-bold fs-5">
                            {{ $bot->bot_name }}'s Subscribers
                        </div>
                    </div>

                    <div class="card-body p-0">
                        @if (session('status'))
                            <div class="alert alert-success rounded-0 m-0" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif

                        <table class="table table-striped table-hover m-0">
                            <thead class="table-dark">
                                <tr>
                                    <th class="text-center" style="width: 5rem;">No.</th>
                                    <th>UID</th>
                                    <th>Chat ID</th>
                                    <th>Username</th>
                                    <th class="text-center" style="width: 15rem;">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @php
                                    $i = 1;
                                @endphp
                                @forelse ($records as $record)
                                    <tr>
                                        <td class="text-center">{{ $i }}</td>
                                        <td>{{ $record->uid }}</td>
                                        <td>{{ $record->chat_id }}</td>
                                        <td>{{ $record->username }}</td>
                                        <td class="text-center">
                                            <a
                                                href="{{ route('bots.subscribers.destroy', [$bot->id, $record->id]) }}"
                                                class="btn btn-danger btn-sm"
                                                onclick="event.preventDefault();document.getElementById('delete-{{$record->id}}').submit();"
                                            >
                                                <i class="fa fa-trash"></i>
                                            </a>
                                            <form
                                                id="delete-{{$record->id}}"
                                                action="{{ route('bots.subscribers.destroy', [$bot->id, $record->id]) }}"
                                                method="post"
                                                class="d-none"
                                            >
                                                @csrf
                                                @method('DELETE')
                                            </form>
                                        </td>
                                    </tr>
                                    @php
                                        $i++;
                                    @endphp
                                @empty
                                    <tr>
                                        <td colspan="99" class="text-center text-muted fst-italic">No Data.</td>
                                    </tr>
                                @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
