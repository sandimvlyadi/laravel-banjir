@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <form action="{{ route('bots.store') }}" method="post">
                        @csrf
                        <div class="card-header d-flex justify-content-between align-items-center">
                            <div class="fw-bold fs-5">
                                {{ __('Add Bot') }}
                            </div>
                        </div>

                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-12 mb-2">
                                    <label for="bot_token">Token</label>
                                    <input type="text" name="bot_token" id="bot_token"
                                        class="form-control @error('bot_token') is-invalid @enderror"
                                        value="{{ old('bot_token') }}" placeholder="Bot Token" required>
                                    @error('bot_token')
                                        <div class="invalid-feedback">
                                            {{ $message }}
                                        </div>
                                    @enderror
                                </div>
                                <div class="col-md-4 mb-2">
                                    <label for="bot_id">ID</label>
                                    <input type="text" name="bot_id" id="bot_id"
                                        class="form-control @error('bot_id') is-invalid @enderror"
                                        value="{{ old('bot_id') }}" placeholder="Bot ID" readonly required>
                                    @error('bot_id')
                                        <div class="invalid-feedback">
                                            {{ $message }}
                                        </div>
                                    @enderror
                                </div>
                                <div class="col-md-4 mb-2">
                                    <label for="bot_name">Name</label>
                                    <input type="text" name="bot_name" id="bot_name"
                                        class="form-control @error('bot_name') is-invalid @enderror"
                                        value="{{ old('bot_name') }}" placeholder="Bot Name" readonly required>
                                    @error('bot_name')
                                        <div class="invalid-feedback">
                                            {{ $message }}
                                        </div>
                                    @enderror
                                </div>
                                <div class="col-md-4 mb-2">
                                    <label for="bot_username">Username</label>
                                    <input type="text" name="bot_username" id="bot_username"
                                        class="form-control @error('bot_username') is-invalid @enderror"
                                        value="{{ old('bot_username') }}" placeholder="Bot Username" readonly required>
                                    @error('bot_username')
                                        <div class="invalid-feedback">
                                            {{ $message }}
                                        </div>
                                    @enderror
                                </div>
                            </div>
                        </div>

                        <div class="card-footer d-flex justify-content-between align-items-center">
                            <a href="{{ route('bots.index') }}" class="btn btn-secondary"><i class="fa fa-chevron-left"></i>
                                Back</a>
                            <button id="submit" type="submit" class="btn btn-success" disabled><i
                                    class="fa fa-save"></i> Save</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        $(function() {
            let timer = 0;
            $('#bot_token').on('input', function() {
                $('#submit').prop('disabled', true);
                $(this).removeClass('is-valid');
                $(this).removeClass('is-invalid');
                $('#bot_id').val('');
                $('#bot_name').val('');
                $('#bot_username').val('');

                clearTimeout(timer);
                timer = setTimeout(() => {
                    axios.post('{{ route('bots.check') }}', {
                        token: $(this).val()
                    }).then(function(response) {
                        if (response.data.data) {
                            $('#submit').prop('disabled', false);
                            $('#bot_token').addClass('is-valid');

                            $('#bot_id').val(response.data.data.id);
                            $('#bot_name').val(response.data.data.first_name);
                            $('#bot_username').val(response.data.data.username);
                        } else {
                            $('#submit').prop('disabled', true);
                            $('#bot_token').addClass('is-invalid');
                        }
                    }).catch(function(error) {
                        $('#submit').prop('disabled', true);
                        $('#bot_token').addClass('is-invalid');
                    });
                }, 500);
            });
        });
    </script>
@endsection
