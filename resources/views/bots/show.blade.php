@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header d-flex justify-content-between align-items-center">
                        <div class="fw-bold fs-5">
                            {{ __('Show Device') }}
                        </div>
                    </div>

                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-12 mb-2">
                                <label for="bot_token">Token</label>
                                <input type="text" name="bot_token" id="bot_token" class="form-control" value="{{ $record->bot_token }}" placeholder="Bot Token" readonly>
                            </div>
                            <div class="col-md-4 mb-2">
                                <label for="bot_id">ID</label>
                                <input type="text" name="bot_id" id="bot_id" class="form-control" value="{{ $record->bot_id }}" placeholder="Bot ID" readonly>
                            </div>
                            <div class="col-md-4 mb-2">
                                <label for="bot_name">Name</label>
                                <input type="text" name="bot_name" id="bot_name" class="form-control" value="{{ $record->bot_name }}" placeholder="Bot Name" readonly>
                            </div>
                            <div class="col-md-4 mb-2">
                                <label for="bot_username">Username</label>
                                <input type="text" name="bot_username" id="bot_username" class="form-control" value="{{ $record->bot_username }}" placeholder="Bot Username" readonly>
                            </div>
                            <div class="col-md-12 mb-2">
                                <label for="uid">UID</label>
                                <input type="text" name="uid" id="uid" class="form-control" value="{{ $record->uid }}" placeholder="UID" readonly>
                            </div>
                        </div>
                    </div>

                    <div class="card-footer d-flex justify-content-between align-items-center">
                        <a href="{{ route('bots.index') }}" class="btn btn-secondary"><i class="fa fa-chevron-left"></i> Back</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
