<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\WebhookController;
use App\Http\Controllers\SendController;
use App\Http\Controllers\BotController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return redirect(route('home'));
});

Route::post('webhooks', [WebhookController::class, 'index'])->name('webhooks.index');
Route::post('send/message', [SendController::class, 'message'])->name('send.message');
Route::post('send/location', [SendController::class, 'location'])->name('send.location');

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::resource('bots', BotController::class);
Route::post('bots/check', [BotController::class, 'check'])->name('bots.check');
Route::get('bots/{bot}/subscribers', [BotController::class, 'subscribers'])->name('bots.subscribers');
Route::delete('bots/{bot}/subscribers/{record}', [BotController::class, 'subscriberDestroy'])->name('bots.subscribers.destroy');
