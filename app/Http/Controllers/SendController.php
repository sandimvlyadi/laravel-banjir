<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

use App\Libraries\Telegram;

use App\Models\Bot;
use App\Models\BotUser;

class SendController extends Controller
{
    public function message(Request $request)
    {
        $uniqid = uniqid();
        $data = [
            'header' => $request->header(),
            'body' => $request->all(),
        ];
        $ymd = date('Ymd');
        Storage::put("send/message/{$ymd}/{$uniqid}.json", json_encode($data, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES));

        $uid = $request->header('x-uid', '');
        $message = $request->input('message', '');
        $target = $request->input('target', '');
        $thread_id = $request->input('thread_id', '');
        $parse_mode = $request->input('parse_mode', 'markdown');
        $disable_web_page_preview = $request->input('disable_web_page_preview', false);
        $disable_notification = $request->input('disable_notification', false);

        if (empty($uid) || empty($message)) {
            return response()->json([
                'status' => 'error',
                'message' => 'Missing required parameters'
            ], 400);
        }

        $bot = Bot::where('uid', $uid)->first();
        if (!$bot) {
            return response()->json([
                'status' => 'error',
                'message' => 'Bot not found'
            ], 404);
        }

        $users = $bot->users;
        if ($target != '') {
            $target = explode(',', $target);
            $users = BotUser::whereIn('chat_id', $target)->where('trans_id', $bot->id)->get();
        }

        $responses = [];
        foreach ($users as $user) {
            $resp = Telegram::sendMessage($bot->bot_token, $user->chat_id, $message, $parse_mode, $disable_web_page_preview, $disable_notification, $thread_id);
            $responses[] = $resp->json();
        }

        return response()->json([
            'status' => 'success',
            'data' => $responses
        ], 200);
    }

    public function location(Request $request)
    {
        $uniqid = uniqid();
        $data = [
            'header' => $request->header(),
            'body' => $request->all(),
        ];
        $ymd = date('Ymd');
        Storage::put("send/location/{$ymd}/{$uniqid}.json", json_encode($data, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES));

        $uid = $request->header('x-uid', '');
        $target = $request->input('target', '');
        $thread_id = $request->input('thread_id', '');
        $lat = $request->input('lat', '');
        $lng = $request->input('lng', '');

        if (empty($uid) || empty($lat) || empty($lng)) {
            return response()->json([
                'status' => 'error',
                'message' => 'Missing required parameters'
            ], 400);
        }

        $bot = Bot::where('uid', $uid)->first();
        if (!$bot) {
            return response()->json([
                'status' => 'error',
                'message' => 'Bot not found'
            ], 404);
        }

        $users = $bot->users;
        if ($target != '') {
            $target = explode(',', $target);
            $users = BotUser::whereIn('chat_id', $target)->where('trans_id', $bot->id)->get();
        }

        $responses = [];
        foreach ($users as $user) {
            $resp = Telegram::sendLocation($bot->bot_token, $user->chat_id, $lat, $lng, $thread_id);
            $responses[] = $resp->json();
        }

        return response()->json([
            'status' => 'success',
            'data' => $responses
        ], 200);
    }
}
