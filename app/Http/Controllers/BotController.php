<?php

namespace App\Http\Controllers;

use Auth;
use App\Libraries\Telegram;
use Illuminate\Http\Request;

use App\Http\Requests\StoreBotRequest;
use App\Http\Requests\UpdateBotRequest;
use App\Models\Bot;
use App\Models\BotUser;

class BotController extends Controller
{
    protected $user;

    public function __construct()
    {
        $this->middleware('auth');

        $this->middleware(function ($request, $next) {
            $this->user = Auth::user();
            return $next($request);
        });

    }

    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $records = Bot::where('user_id', $this->user->id)
            ->orderBy('id', 'desc')
            ->get();

        return view('bots.index', [
            'records' => $records
        ]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('bots.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreBotRequest $request)
    {
        $record = new Bot;
        $record->bot_token = $request->bot_token;
        $record->bot_id = $request->bot_id;
        $record->bot_name = $request->bot_name;
        $record->bot_username = $request->bot_username;
        $record->user_id = $this->user->id;
        $record->save();

        $record = Bot::find($record->id);

        $webhookURL = env("WEBHOOK_URL");
        Telegram::setWebhook($record->bot_token, $webhookURL, true, $record->uid);
        Telegram::setMyCommands($record->bot_token, [
            [
                "command" => "/start",
                "description" => "Subscribe to the bot"
            ],
            [
                "command" => "/threadid",
                "description" => "Get the thread ID"
            ],
        ]);

        return redirect(route('bots.index'))->with('status', 'New Bot has been added.');
    }

    /**
     * Display the specified resource.
     */
    public function show(Bot $bot)
    {
        if ($this->user->id != $bot->user_id) {
            abort(404);
        }

        return view('bots.show', [
            'record' => $bot
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Bot $bot)
    {
        if ($this->user->id != $bot->user_id) {
            abort(404);
        }

        return view('bots.edit', [
            'record' => $bot
        ]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateBotRequest $request, Bot $bot)
    {
        if ($this->user->id != $bot->user_id) {
            abort(401);
        }

        $bot->bot_token = $request->bot_token;
        $bot->bot_id = $request->bot_id;
        $bot->bot_name = $request->bot_name;
        $bot->bot_username = $request->bot_username;
        $bot->save();

        return redirect(route('bots.index'))->with('status', 'Bot has been updated.');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Bot $bot)
    {
        if ($this->user->id != $bot->user_id) {
            abort(401);
        }

        $bot->delete();

        return redirect(route('bots.index'))->with('status', 'Bot has been deleted.');
    }

    public function check(Request $request)
    {
        $token = $request->input('token', '');

        $response = Telegram::getMe($token);
        $response = $response->json();

        if ($response['ok']) {
            return response()->json([
                'message' => 'Ok',
                'data' => $response['result']
            ], 200);
        }

        return response()->json([
            'message' => 'Not Found',
            'data' => null
        ], 404);
    }

    public function subscribers(Bot $bot)
    {
        if ($this->user->id != $bot->user_id) {
            abort(404);
        }

        $subscribers = $bot->users()->orderBy('id', 'desc')->get();

        return view('bots.subscribers', [
            'bot' => $bot,
            'records' => $subscribers
        ]);
    }

    public function subscriberDestroy(Bot $bot, BotUser $record)
    {
        if ($this->user->id != $bot->user_id) {
            abort(401);
        }

        $record->delete();

        return redirect(route('bots.subscribers', $bot->id))->with('status', 'Subscriber has been deleted.');
    }
}
