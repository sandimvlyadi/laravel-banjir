<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

use App\Libraries\Telegram;

use App\Models\Bot;
use App\Models\BotUser;

class WebhookController extends Controller
{
    public function index(Request $request)
    {
        $uniqid = uniqid();
        $data = [
            'header' => $request->header(),
            'body' => $request->all(),
        ];
        $ymd = date('Ymd');
        Storage::put("webhooks/{$ymd}/{$uniqid}.json", json_encode($data, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES));

        $uid = $request->header('x-telegram-bot-api-secret-token', '');
        $message = $request->input('message', null);
        $my_chat_member = $request->input('my_chat_member', null);
        $channel_post = $request->input('channel_post', null);
        $token = '';

        if ($uid != '' && !is_null($message)) {
            $bot = Bot::where('uid', $uid)->first();
            if ($bot) {
                $token = $bot->bot_token;
                if (isset($message['chat'])) {
                    $chat_id = $message['chat']['id'];
                    $status = 1;
                    $username = '';
                    $isGroup = false;
                    $isUsername = false;
                    if ($message['chat']['type'] == 'private') {
                        if (isset($message['chat']['username'])) {
                            $username = $message['chat']['username'];
                            $isUsername = true;
                        } else {
                            $username = $message['chat']['first_name'] . ' ' . $message['chat']['last_name'];
                        }
                    } elseif ($message['chat']['type'] == 'group' || $message['chat']['type'] == 'supergroup') {
                        $username = $message['chat']['title'];
                        $isGroup = true;
                    }

                    $user = BotUser::where('trans_id', $bot->id)->where('chat_id', $chat_id)->first();
                }

                if (isset($message['text'])) {
                    $msg = trim(strtolower($message['text']));
                    if ($msg == '/start') {
                        if ($user) {
                            if (!$isGroup) {
                                Telegram::sendMessage($token, $chat_id, "You've been subscribed.");
                            } else {
                                Telegram::sendMessage($token, $chat_id, "This group has been subscribed.");
                            }
                        } else {
                            foreach ($bot->users as $user) {
                                if ($user->username == 'sandimvlyadi') {
                                    if ($isUsername) {
                                        Telegram::sendMessage($token, $user->chat_id, "New user @{$username} has been subscribed to *{$bot->name}*.");
                                    } else {
                                        if (!$isGroup) {
                                            Telegram::sendMessage($token, $user->chat_id, "New user *{$username}* has been subscribed to *{$bot->name}*.");
                                        } else {
                                            Telegram::sendMessage($token, $user->chat_id, "*{$bot->name}* has been added to group *{$username}*.");
                                        }
                                    }
                                }
                            }

                            $user = new BotUser;
                            $user->trans_id = $bot->id;
                            $user->chat_id = $chat_id;
                            $user->username = $username;
                            $user->save();

                            if (!$isGroup) {
                                Telegram::sendMessage($token, $chat_id, "You've subscribed.\nYou'll receive notification from this bot in the future.");
                            } else {
                                Telegram::sendMessage($token, $chat_id, "Thank you.\nThis group will receive notification from this bot in the future.", 'html');
                            }
                        }
                    } elseif ($msg == 'ping' || $msg == 'p') {
                        Telegram::sendMessage($token, $chat_id, "PONG");
                    } elseif ($msg == '/threadid') {
                        if (isset($message['message_thread_id'])) {
                            $thread_id = $message['message_thread_id'];
                            Telegram::sendMessage($token, $chat_id, "THREAD ID: `{$thread_id}`.", 'markdown', false, false, $thread_id);
                        } else {
                            Telegram::sendMessage($token, $chat_id, "This chat isn't a thread.");
                        }
                    }

                    return response()->json([
                        'status' => true,
                        'message' => [
                            'icon' => 'success',
                            'title' => 'Success!',
                            'text' => 'Your request has been accepted.'
                        ]
                    ], 200);
                }
            } else {
                return response()->json([
                    'status' => false,
                    'message' => [
                        'icon' => 'error',
                        'title' => 'Failed!',
                        'text' => 'Bot is not found.'
                    ]
                ], 404);
            }
        }

        if ($uid != '' && !is_null($my_chat_member)) {
            $bot = Bot::where('uid', $uid)->first();
            if ($bot) {
                $token = $bot->bot_token;
                if (isset($my_chat_member['new_chat_member'])) {
                    if (isset($my_chat_member['new_chat_member']['status'])) {
                        $status = $my_chat_member['new_chat_member']['status'];
                        $chat_id = $my_chat_member['chat']['id'];

                        if ($status == 'member' || $status == 'administrator') {
                            $username = '';
                            if (isset($my_chat_member['from']['username'])) {
                                $username = '@' . $my_chat_member['from']['username'];
                            } else {
                                $username = $my_chat_member['from']['first_name'] . ' ' . $my_chat_member['from']['last_name'];
                            }

                            if ($my_chat_member['new_chat_member']['user']['username'] == $bot->username) {
                                Telegram::sendMessage($token, $chat_id, "Thank you {$username} for adding me to this group.\nPlease use /start command to begin receiving notification from this bot.");
                            }
                        } elseif ($status == 'left' || $status == 'kicked') {
                            BotUser::where('trans_id', $bot->id)->where('chat_id', $chat_id)->delete();
                        }

                        return response()->json([
                            'status' => true,
                            'message' => [
                                'icon' => 'success',
                                'title' => 'Success!',
                                'text' => 'Your request has been accepted.'
                            ]
                        ], 200);
                    }
                }
            } else {
                return response()->json([
                    'status' => false,
                    'message' => [
                        'icon' => 'error',
                        'title' => 'Failed!',
                        'text' => 'Bot is not found.'
                    ]
                ], 404);
            }
        }

        if ($uid != '' && !is_null($channel_post)) {
            $bot = Bot::where('uid', $uid)->first();
            if ($bot) {
                $token = $bot->bot_token;
                if (isset($channel_post['chat'])) {
                    $chat_id = $channel_post['chat']['id'];
                    $username = $channel_post['chat']['title'];

                    $user = BotUser::where('trans_id', $bot->id)->where('chat_id', $chat_id)->first();
                }

                if (isset($channel_post['text'])) {
                    $msg = trim(strtolower($channel_post['text']));
                    if ($msg == '/start') {
                        if ($user) {
                            Telegram::sendMessage($token, $chat_id, "This channel has been subscribed.");
                        } else {
                            foreach ($bot->users as $user) {
                                if ($user->username == 'sandimvlyadi') {
                                    Telegram::sendMessage($token, $user->chat_id, "Channel @{$username} has been subscribed to *{$bot->name}*.");
                                }
                            }

                            $user = new BotUser;
                            $user->trans_id = $bot->id;
                            $user->chat_id = $chat_id;
                            $user->username = $username;
                            $user->save();

                            Telegram::sendMessage($token, $chat_id, "Thank you.\nThis channel will receive notification from this bot in the future.", 'html');
                        }
                    } elseif ($msg == 'ping' || $msg == 'p') {
                        Telegram::sendMessage($token, $chat_id, "PONG");
                    } elseif ($msg == '/threadid') {
                        if (isset($message['message_thread_id'])) {
                            $thread_id = $message['message_thread_id'];
                            Telegram::sendMessage($token, $chat_id, "THREAD ID: `{$thread_id}`.", 'markdown', false, false, $thread_id);
                        } else {
                            Telegram::sendMessage($token, $chat_id, "This chat isn't a thread.");
                        }
                    }

                    return response()->json([
                        'status' => true,
                        'message' => [
                            'icon' => 'success',
                            'title' => 'Success!',
                            'text' => 'Your request has been accepted.'
                        ]
                    ], 200);
                }
            } else {
                return response()->json([
                    'status' => false,
                    'message' => [
                        'icon' => 'error',
                        'title' => 'Failed!',
                        'text' => 'Bot is not found.'
                    ]
                ], 404);
            }
        }
    }
}
