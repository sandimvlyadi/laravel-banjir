<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Bot extends Model
{
    use HasFactory;

    public function users()
    {
        return $this->hasMany(BotUser::class, 'trans_id');
    }

    public function owner()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
