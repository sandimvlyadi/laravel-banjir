<?php
namespace App\Libraries;

use Storage;
use Illuminate\Support\Facades\Http;

class Telegram {
    const API = 'https://api.telegram.org';
    const FAILED = [
        'ok' => false,
        'result' => null
    ];

    public static function getMe( $token = '' )
    {
        $api = self::API;
        $response = Http::withOptions([
            'verify' => false,
        ])->get("{$api}/bot{$token}/getMe");

        return $response;
    }

    public static function getUpdates( $token = '' )
    {
        $api = self::API;
        $response = Http::withOptions([
            'verify' => false,
        ])->get("{$api}/bot{$token}/getUpdates");

        return $response;
    }

    public static function isExistSubscribers( $arrs = [], $chat_id = '' )
    {
        foreach ($arrs as $arr) {
            if ($arr['chat_id'] == $chat_id) {
                return true;
            }
        }

        return false;
    }

    public static function getSubscribers( $token = '' )
    {
        $response = self::getUpdates( $token );
        $response = $response->json();

        if ($response['ok']) {
            $results = $response['result'];

            $data = [];
            foreach ($results as $result) {
                // group
                if (isset($result['my_chat_member']) && isset($result['my_chat_member']['chat']['title'])) {
                    $temp = [
                        'chat_id'       => $result['my_chat_member']['chat']['id'],
                        'username'      => $result['my_chat_member']['chat']['title'],
                        'status'        => 1
                    ];

                    $isExist = self::isExistSubscribers($data, $temp['chat_id']);
                    if (!$isExist) {
                        array_push($data, $temp);
                    }
                }

                // contact
                if (isset($result['message']) && isset($result['message']['text'])) {
                    if ($result['message']['text'] == '/start') {
                        $temp = [
                            'chat_id'       => $result['message']['from']['id'],
                            'username'      => $result['message']['from']['username'],
                            'status'        => 1
                        ];

                        $isExist = self::isExistSubscribers($data, $temp['chat_id']);
                        if (!$isExist) {
                            array_push($data, $temp);
                        }
                    }
                }
            }

            return [
                'ok' => true,
                'result' => $data
            ];
        }

        return [
            'ok' => false,
            'result' => []
        ];
    }

    public static function sendMessage( $token = '', $chat_id = '', $text = '', $parseMode = 'markdown', $disableWebPagePreview = false, $disableNotification = false, $thread_id = '' )
    {
        $api = self::API;
        $response = Http::withOptions([
            'verify' => false,
        ])
        ->post("{$api}/bot{$token}/sendMessage", [
            'chat_id' => $chat_id,
            'message_thread_id' => $thread_id,
            'text' => $text,
            'parse_mode' => $parseMode,
            'disable_web_page_preview' => $disableWebPagePreview,
            'disable_notification' => $disableNotification
        ]);

        return $response;
    }

    public static function sendDocument( $token = '', $chat_id = '', $caption = '', $filepath = '', $filename = '', $parseMode = 'markdown', $disableNotification = false, $thread_id = '' )
    {
        $api = self::API;
        $response = Http::withOptions([
            'verify' => false,
        ])
        ->attach(
            'document', Storage::disk('local')->get($filepath), $filename
        )
        ->post("{$api}/bot{$token}/sendDocument", [
            'chat_id' => $chat_id,
            'message_thread_id' => $thread_id,
            'caption' => $caption,
            'parse_mode' => $parseMode,
            'disable_notification' => $disableNotification
        ]);

        return $response;
    }

    public static function sendLocation( $token = '', $chat_id = '', $lat = '', $lon = '', $thread_id = '' )
    {
        $api = self::API;
        $response = Http::withOptions([
            'verify' => false,
        ])
        ->post("{$api}/bot{$token}/sendLocation", [
            'chat_id' => $chat_id,
            'message_thread_id' => $thread_id,
            'latitude' => $lat,
            'longitude' => $lon
        ]);

        return $response;
    }

    public static function setWebhook( $token = '', $url = '', $dropPendingUpdates = true, $secret_token = '' )
    {
        $api = self::API;
        $response = Http::withOptions([
            'verify' => false,
        ])
        ->post("{$api}/bot{$token}/setWebhook", [
            'url' => $url,
            'secret_token' => $secret_token,
            'drop_pending_updates' => $dropPendingUpdates
        ]);

        return $response;
    }

    public static function getWebhookInfo( $token = '' )
    {
        $api = self::API;
        $response = Http::withOptions([
            'verify' => false,
        ])
        ->get("{$api}/bot{$token}/getWebhookInfo");

        return $response;
    }

    public static function deleteWebhook( $token = '', $dropPendingUpdates = true )
    {
        $api = self::API;
        $response = Http::withOptions([
            'verify' => false,
        ])
        ->post("{$api}/bot{$token}/deleteWebhook", [
            'drop_pending_updates' => $dropPendingUpdates
        ]);

        return $response;
    }

    public static function setMyCommands( $token = '', $commands = '' )
    {
        $api = self::API;
        $response = Http::withOptions([
            'verify' => false,
        ])
        ->post("{$api}/bot{$token}/setMyCommands", [
            'commands' => $commands
        ]);

        return $response;
    }

    public static function changeWebhook( $token = '', $url = '', $dropPendingUpdates = true, $ip_address = '' )
    {
        $api = self::API;
        $response = Http::withOptions([
            'verify' => false,
        ])
        ->post("{$api}/bot{$token}/setWebhook", [
            'url'                   => $url,
            'ip_address'            => $ip_address,
            'drop_pending_updates'  => $dropPendingUpdates
        ]);

        return $response;
    }
}
